#######
## Builder to generate the binary
#######
FROM golang:alpine as builder

COPY main.go main.go
RUN go build main.go

#######
## The final image that is used to run the binary. Based on scratch to be really lightweight
#######
FROM alpine
WORKDIR /app
COPY --from=builder /go/main /app/goapp
ENTRYPOINT ./goapp