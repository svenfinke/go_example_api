IMAGENAME = example
VERSION = v1
OSINFO := $(shell uname -a)

# WSL can access binaries in the windows $PATH, but you'll have to add '.exe' to the binaries name. If you want to use
# e.g. the hosts `docker ..`, you'll have to use `docker.exe ..` instead.
ifneq (,$(findstring Microsoft,$(OSINFO)))
    WSLFIX = .exe
else
    WSLFIX =
endif

build:
	docker$(WSLFIX) build -t $(IMAGENAME):$(VERSION) .
deploy:
	kubectl$(WSLFIX) apply -f k8s/deployment.yaml
	kubectl$(WSLFIX) apply -f k8s/service.yaml
proxy:
	echo 'open http://localhost:8001/api/v1/namespaces/default/services/example-api:/proxy/'
	kubectl$(WSLFIX) proxy