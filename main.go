package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {
	log.Printf("main: starting HTTP server")

	startHttpServer()
}

func startHttpServer() {
	srv := &http.Server{Addr: ":80"}

	http.HandleFunc("/api/apples", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Getting Apples.")
	})

	http.HandleFunc("/api/bananas", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Getting Bananas.")
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Getting something awesome.")
	})

	go func() {
		// returns ErrServerClosed on graceful close
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			// NOTE: there is a chance that next line won't have time to run,
			// as main() doesn't wait for this goroutine to stop. don't use
			// code with race conditions like these for production. see post
			// comments below on more discussion on how to handle this.
			log.Fatalf("ListenAndServe(): %s", err)
		}
	}()

	// Setting up signal capturing
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	// Waiting for SIGINT (pkill -2)
	<-stop

	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	if err := srv.Shutdown(ctx); err != nil {
		// handle err
	}
}
